
La antigua Germania de los bárbaros que atacaban al Imperio romano dio paso en la Edad Media a una sociedad feudal, en la que los grandes señores de la época construyeron majestuosas fortalezas para proteger sus dominios. Hoy en Civitatis os presentamos los 10 castillos más bonitos de Alemania, donde podréis dejar volar vuestra imaginación como si fuerais un noble caballero o princesa.

Gran parte de los castillos más bellos de Alemania se encuentran en la región de Renania del Norte-Westfalia, pero también hay otros muchos que se ubican, por ejemplo, en las estribaciones de los Alpes.

cambios agregados
13 de octubre